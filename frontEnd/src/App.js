import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import React from "react";
import { createBrowserRouter,RouterProvider} from "react-router-dom";

import Catalog from "./pages/Catalog";
import Home from "./pages/Home";
import Detail from "./pages/detail/Detail";
import ErrorPage from "./pages/error-page";
import Login from "./pages/Login";
import Layout from "./pages/Root"
import Register from"./pages/Register";
// import Login from "./pages/Login";

// use reducer
import AuthProvider from "./utils/AuthProvider";
import LoginRedicet from "./pages/LoginRedirect";
import { CheckLocalId } from "./utils/useLoaderData";
const App = () => {
  
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      errorElement:<ErrorPage/>,
      loader:CheckLocalId,
      children: [
        {
          index:true,
          path:"/",
          element:<Home/>
          // element:<Login/>
        },
       
        {
          path: "/home",
          element:<Home/>
          // element:<Login/>
        },
        {
          path: "/:category",
          // index:true,
          element:<Catalog/>
        },
        {
          path: "/:category/:id",
          element:<Detail/>
        },
        
        
      ],
    },
    {
      path:"/login",
      element:<Login/>
    },
    {
      path:"/register",
      element:<Register/>
    },
    {
      path:"/loginRedirect",
      element:<LoginRedicet/>
    }
  ]);

  return (
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  )
};
export default App;
