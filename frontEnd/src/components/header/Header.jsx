import React, { useRef, useEffect } from 'react';
// import authContenxt from '../../utils/auth-context';
import { Link, useLocation, useLoaderData } from 'react-router-dom';

import "./dropdown.css";
// import logo from '../../assets/tmovie.png';

const headerNav = [
    {
        display: 'Home',
        path: '/home'
    },
    {
        display: 'Movies',
        path: '/movie'
    },
    {
        display: 'TV Series',
        path: '/tv'
    }
];

const Header = () => {
    document.body.classList.remove("body"); //handle css got something wrong
    const { pathname } = useLocation();

    const getDataUser = useLoaderData();
    console.log(getDataUser);
    // const getDataUser=false;
    const headerRef = useRef();
    // console.log(headerRef);
    const active = headerNav.findIndex(e => e.path === pathname);
    useEffect(() => {
        const shrinkHeader = () => {
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
                headerRef.current.classList.add('shrink');

            } else {
                headerRef.current.classList.remove("shrink");
            }
        }
        window.addEventListener('scroll', shrinkHeader);
        return () => {
            window.removeEventListener('scroll', shrinkHeader);
        };
    }, []);


    // handle logout
    const logoutHandler = (e) => {
        e.preventDefault();
        localStorage.removeItem("tokenJWT");
        localStorage.removeItem("tokenJWTForDummy");
        window.open("http://localhost:5000/auth/logout", "_self");
    }
    return (
        <div ref={headerRef} className="header">
            <div className="header__wrap container">
                <div className="helperStyleMobile" >
                    {/* <img src={logo} alt="" /> */}
                    {/* <Link to="/">H</Link> */}
                   
                </div>
                <div className="logo">
                    {/* <img src={logo} alt="" /> */}
                    <Link to="/">Hesoyam</Link>
                   
                </div>
                <ul className="header__nav">
                    {
                        headerNav.map((e, i) => (
                            <li key={i} className={`${i === active ? 'active' : ''}`}>
                                <Link to={e.path}>
                                    {e.display}
                                </Link>
                            </li>
                        ))
                    }
                </ul>
                {
                    !getDataUser &&
                    <div style={{ display: 'flex', columnGap: "2rem" }}>
                        <Link style={{ padding: ".5rem", borderRadius: "1rem", width: '100%', display: 'inline-block', backgroundColor: "darkblue" }} to={"/login"}><h1>Login</h1></Link>
                        <Link style={{ padding: ".5rem", borderRadius: "1rem", width: '100%', display: 'inline-block', backgroundColor: "darkblue" }} to={"/register"}><h1>Register</h1></Link>
                    </div>

                }
                {getDataUser &&
                    <div className=' dropdown'>
                        {/* <button className='dropbtn'>tes</button> */}
                        <img className='dropbtn' src={getDataUser.user.photos[0].value}      alt="" />
                        <div className="dropdown-content">
                            <p style={{ display: "inline-block", width: "100%",textAlign:"center"}}>{getDataUser.user.name.givenName}</p>
                            <Link style={{width: '100%' }} onClick={logoutHandler}><h1 className='text-center'>Logout</h1></Link>
                            
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

export default Header;
