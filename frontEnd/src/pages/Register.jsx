import { Link } from "react-router-dom";
import "../login.css";
import logo from "../assets/movie.png";
const Register = (props) => {
  document.body.classList.add("body");
  return (
    <>
      <div className="container-full w-full h-[100vh]">
        <div className="lg:grid lg:grid-cols-[45%,55%] lg:justify-center lg:h-[100vh] w-full">
          <div className="w-full h-full  lg:overflow-y-scroll scrollbar lg:pt-20  grid grid-cols-1 justify-center grid-rows-[10rem] lg:grid-rows-[15rem] pb-20 pt-10 box-border bg-page-login">
            <div id="logo" className="flex justify-center">
              <img
                src={logo}
                className="h-32 md:h-40 lg:h-52"
                alt=""
              />
            </div>
            <h2 className="text-center text-2xl font-bold  lg:text-3xl">
              Welcome To Movie
            </h2>
            <div className="px-4 lg:px-8 mt-4">
              <p className="text-center font-normal text-base lg:text-md">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
              <p className="text-center font-normal text-base lg:text-md mt-2">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
              <p className="text-center font-normal text-base lg:text-md mt-2">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
            </div>
          </div>
          <div className="w-full mt-14 pb-20 flex flex-col gap-y-8 justify-center items-center">
            <div id="title-login">
              <h2 className="font-semibold text-xl text-center  lg:text-2xl">
                Register 
              </h2>
              <h2 className="font-semibold text-xl text-center uppercase lg:text-2xl">
                <span className="text-main tracking-wider">HESOYAM</span>{" "}
              </h2>
            </div>
            <div id="form" className="md:w-[60%]">
              <form action="" className="w-full">
              <div className="mt-2">
                  <label
                    
                    className="font-semibold text-smlg:text-medium  mb-2"
                  >
                    First Name
                  </label>
                  <input
                    type="text"
                    name="firstName"
                    className="block w-full px-4 py-2 rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                  />
                  <span
                    id="usernameMessage"
                    className="hidden text-pink-500 text-sm"
                  ></span>
                </div>
                <div className="mt-6">
                  <label
                    className="font-semibold text-smlg:text-medium  mb-2"
                  >
                    Last Name
                  </label>
                  <input
                    type="text"
                    id="lastName"
                    name="lastName"
                    className="block w-full px-4 py-2 rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                  />
                  <span
                    id="usernameMessage"
                    className="hidden text-pink-500 text-sm"
                  ></span>
                </div>
                <div className="mt-6">
                  <label
                    for="Username"
                    className="font-semibold text-smlg:text-medium  mb-2"
                  >
                    Email
                  </label>
                  <input
                    type="text"
                    id="username"
                    name="username"
                    className="block w-full px-4 py-2 rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                  />
                  <span
                    id="usernameMessage"
                    className="hidden text-pink-500 text-sm"
                  ></span>
                </div>
                
                <div className="mt-6">
                  <label
                    for="Username"
                    className="font-semibold text-sm lg:text-medium mb-2"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    className="block px-4 py-2 w-full rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                  />
                  <span
                    id="passwordMessage"
                    className="hidden text-pink-500  text-sm "
                  ></span>
                </div>

                <button
                  id="submit"
                  type="submit"
                  className="  px-4 py-2  mt-8 w-full text-base font-semibold  bg-main rounded-md border-none text-white btn-flash  after:rounded-md after:hover:w-full"
                >
                  Daftar Sekarang
                </button>
                
                <div className="mt-6 flex justify-center">
                  <Link to="/login">Already have account</Link>
                </div>
             
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Register;
