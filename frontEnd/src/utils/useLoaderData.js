const getData = async () => {
    try {
        const request = await fetch("http://localhost:5000/auth/login/success", {
            method: "GET",
            credentials: "include",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Credentials": true,
            }
        });
        if (request.status === 200) {
            const getData = await request.json();
            return getData;
        
        }
        throw new Error("Opps,Error when request")
        // console.log(request);
    } catch (e) {
        console.log(e.message);
    }
}
export const CheckLocalId=async()=>{
    const checkToken=localStorage.getItem("tokenJWT");
    const checkTokenDummy=localStorage.getItem("tokenJWTForDummy");
    console.log(checkTokenDummy);
    if(checkToken){
        return (await getData());
    }
    if(checkTokenDummy){
        return {user:{
            name:{
                givenName:"Dummy"
            },
            photos:[{
                value:"https://flowbite.com/docs/images/people/profile-picture-5.jpg"
            }]
        }}
    };
    return null;
}