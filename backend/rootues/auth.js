import router from "express";
import passport from "passport";
import dotenv from "dotenv";
import jwt from "jsonwebtoken"  
const env=dotenv.config(); //get access to file .env

const Router=router.Router();
Router.get("/google",passport.authenticate("google",{
    scope:["Profile"]
}));
Router.get("/login/success",(req,res)=>{
    console.log(req.user);
    if(req.user){
        // generate token 
        const token= jwt.sign(
            { user_id: req.user.id, displayName:req.user.displayName },
            env.parsed.ACCESS_TOKEN_SECRET,
            {
              expiresIn: "2h",
            });
            // console.log(token);
        res.status(200).json({
            success:true,
            user:req.user,
            tokenJWT:token,
            message:"your request is successfully"
        })
    }
    
})
Router.get("/logout",(req,res)=>{
    req.logout();
    res.redirect("http://localhost:3000/login")
})
Router.get("/login/failed",(req,res)=>{
    res.status(401).json({
        success:false,
        message:"Opps field to authenticate with goolge"
    })
})
// define 
Router.get("/google/callback",passport.authenticate("google",{
    successRedirect:"http://localhost:3000/loginRedirect",
    failureRedirect:"/login/failed"

}))

export default Router;