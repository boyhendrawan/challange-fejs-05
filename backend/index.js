import express from "express";
import cookieSession from "cookie-session";
import passport from "passport";
import cors from "cors";
import"./passort.js";
import authRouter from "./rootues/auth.js"
// console.log("change");
const server=express();

server.use(cookieSession({
    name:"session",
    keys:["hesoyam"],
    maxAge:24*60*60*1000
}))
// use passport js
server.use(passport.initialize());
server.use(passport.session());

// use cors for allow all request
server.use(cors({
    origin:"http://localhost:3000",
    methods:"GET,POST,PUT,DELETE",
    credentials:true
}))
// define rooters
server.use("/auth",authRouter)
server.listen(5000,()=>console.log("WOrks"))